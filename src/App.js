import React from "react";
import NavBar from "./common/NavBar";
import TableComp from "./common/Table";
import TableFit from "./common/TableFit";
import Graph from "./common/graph";

export default class App extends React.Component {
  state = {
    population: [],
    bestSurvivedAllRun: [],
    bestSurvived: [],
    selected: [],
    generation: 1,
    run: 1
  };
  componentDidMount() {}
  render() {
    return (
      <div
        className="App"
        style={{
          display: "flex",
          flexDirection: "column",
          width: window.innerWidth,
          height: window.innerHeight,
          backgroundColor: "#212121"
        }}
      >
        <link
          rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous"
        />
        <NavBar />
        <button
          style={{
            marginLeft: "auto",
            marginRight: "auto",
            marginTop: "10px",
            right: "10px",
            border: "none",
            borderRadius: "20px",
            outline: "none",
            width: "100px"
          }}
          onClick={() => this.initialize()}
        >
          Initialize
        </button>

        <div
          style={{
            width: "100%",
            marginTop: 20,
            height: "100%",
            padding: 50,
            overflow: "scroll",
            overflowX: "hidden"
          }}
        >
          <div style={{ color: "#fff", margin: "10px" }}>Data</div>
          <TableComp data={[]} survived={this.state.bestSurvivedAllRun} />
          <TableFit data={[]} survived={this.state.bestSurvivedAllRun} />
          <Graph survived={this.state.bestSurvivedAllRun} />
          <div>
            <h3>Average BSF = Blue</h3>
          </div>
          <div>
            <h3>Average Avg. Fitness = Red</h3>
          </div>
        </div>
      </div>
    );
  }

  fitnessProportionateScheme(population, offSpring) {
    //Sum of Fitness Array
    let fitnessSum = 0;
    //Array of Probabilities of Fitness
    let fitnessProbability = [];

    let selected = [];

    //Sort Fitness Array in Ascending Order
    population.sort((a, b) => {
      return a.fitness - b.fitness;
    });

    //Calculate Fitness Sum
    population.forEach(pop => {
      fitnessSum += pop.fitness;
    });

    //Calculate Probability For Each Fitness
    population.forEach(pop => {
      fitnessProbability.push(pop.fitness / fitnessSum);
    });

    for (let j = 0; j < 10; j++) {
      //Generate Random Number Between 0 and 1
      let randomNumber = Math.random();

      //Sum of Probability (Used To Define Ranges For Comparision), Initially = Probability of the first Fitness
      let probabilitySum = fitnessProbability[0];

      //Compare random number with ranges and select the matching one
      for (let i = 0; i < population.length; i++) {
        if (randomNumber < probabilitySum) {
          selected.push(population[i]);
          break;
        } else probabilitySum += fitnessProbability[i + 1];
      }
    }
    this.state.selected = selected;
    if (offSpring) {
      this.setState({
        bestSurvived: [
          ...this.state.bestSurvived,
          {
            best: population[population.length - 1],
            avg: fitnessSum / 10
          }
        ]
      });
    }
  }

  truncationScheme(population, offSpring) {
    let fitnessSum = 0;
    population.forEach(pop => {
      fitnessSum += pop.fitness;
    });
    population.sort((a, b) => {
      return b.fitness - a.fitness;
    });
    let selected = [];
    selected = population.slice(0, 10);
    this.state.selected = selected;

    if (offSpring) {
      this.setState({
        bestSurvived: [
          ...this.state.bestSurvived,
          {
            best: population[0],
            avg: fitnessSum / 10
          }
        ]
      });
    }
  }

  binaryTournamentScheme(population, offSpring) {
    let selected = [];
    for (let i = 0; i < 10; i++) {
      let firstRandomSelection =
        population[Math.floor(Math.random() * population.length)];
      let SecondRandomSelection =
        population[Math.floor(Math.random() * population.length)];

      if (firstRandomSelection > SecondRandomSelection)
        selected.push(firstRandomSelection);
      else selected.push(SecondRandomSelection);
    }
    this.state.selected = selected;

    let fitnessSum = 0;
    population.forEach(pop => {
      fitnessSum += pop.fitness;
    });
    population.sort((a, b) => {
      return b.fitness - a.fitness;
    });

    if (offSpring) {
      this.setState({
        bestSurvived: [
          ...this.state.bestSurvived,
          {
            best: population[0],
            avg: fitnessSum / 10
          }
        ]
      });
    }
  }

  randomSelectionScheme(population, offSpring) {
    let selected = [];
    for (let i = 0; i < 10; i++) {
      selected.push(population[Math.floor(Math.random() * population.length)]);
    }
    this.state.selected = selected;

    let fitnessSum = 0;
    population.forEach(pop => {
      fitnessSum += pop.fitness;
    });
    population.sort((a, b) => {
      return b.fitness - a.fitness;
    });

    if (offSpring) {
      this.setState({
        bestSurvived: [
          ...this.state.bestSurvived,
          {
            best: population[0],
            avg: fitnessSum / 10
          }
        ]
      });
    }
  }

  async initialize() {
    if (!this.state.population.length) {
      let population = [];
      for (let i = 0; i < 10; i++) {
        population.push({
          x: this.getRandomInt(-4, 4),
          y: this.getRandomInt(-4, 4),
          fitness: 0
        });
      }

      await this.setState({ population: population });
    }
    this.calculateFitness(false);
    this.binaryTournamentScheme(this.state.population, false);
    this.crossover();
    this.mutate();
    this.calculateFitness(true);
    this.truncationScheme(
      this.state.population.concat(this.state.selected),
      true
    );
    this.nextGeneration();
  }

  async nextGeneration() {
    if (this.state.generation <= 40) {
      await this.setState({ generation: this.state.generation + 1 });
      await this.setState({
        population: this.state.selected
      });
      this.initialize();
    } else {
      this.nexRun();
    }
  }

  async nexRun() {
    if (this.state.run <= 10) {
      this.state.run++;
      this.state.population = [];
      this.state.selected = [];
      await this.setState({
        bestSurvivedAllRun: [
          ...this.state.bestSurvivedAllRun,
          this.state.bestSurvived
        ]
      });
      await this.setState({ bestSurvived: [] });
      this.state.generation = 1;
      this.initialize();
    }
  }

  async calculateFitness(offSpring) {
    if (this.state.population) {
      let pops = [...this.state.population];

      for (let i = 0; i < 10; i++) {
        pops[i].fitness = pops[i].x * pops[i].x + pops[i].y * pops[i].y;
      }
      this.setState({ population: pops });
    }
  }
  crossover() {
    let population = this.state.selected;

    for (let i = 0; i < 9; i++) {
      let populationTemp = population[i];
      population[i].x = population[i + 1].y;
      population[i].y = population[i + 1].x;
      population[i + 1].x = populationTemp.y;
      population[i + 1].y = populationTemp.x;
    }
    this.state.selected = population;
  }

  mutate() {
    let population = this.state.selected;

    for (let i = 0; i < 10; i++) {
      if (i < 5) {
        if (Math.ceil(Math.random() * 10) % 2) {
          population[i].x += 0.25;
        } else population[i].y += 0.25;
      } else {
        if (Math.ceil(Math.random() * 10) % 2) {
          population[i].x -= 0.25;
        } else population[i].y -= 0.25;
      }
    }

    this.state.selected = population;
  }

  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}
