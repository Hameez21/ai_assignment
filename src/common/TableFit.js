import React, { Component } from "react";
import Table from "react-bootstrap/Table";

export default class TableFit extends Component {
  state = {
    generation: this.props.data,
    rowGenerator: [
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1"
    ],
    first: true,
    avg: []
  };

  componentWillReceiveProps(nextProps) {
    const { survived } = nextProps;
    let sum = 0;
    let avg = [];
    if (survived.length === 10) {
      if (survived[9].length === 41) {
        if (this.state.first) {
          this.setState({ first: false }, () => {
            for (let i = 0; i < 40; i++) {
              survived.map((item, index) => {
                sum += item[i].avg;
              });
              console.log(sum);
              avg.push(sum / 10);
              sum = 0;
            }
          });
          // for (let i = 0; i < 1; i++)
          //   survived.map((item, index) => {
          //     console.log(item[i].best.fitness);
          //   });
          this.setState({ avg: avg });
        }
      }
    }
  }
  render() {
    let table = this.props.survived.map((item, index) => {
      return (
        <td>
          <tr>{item[0].avg}</tr>
          <tr>{item[1].avg}</tr>
          <tr>{item[2].avg}</tr>
          <tr>{item[3].avg}</tr>
          <tr>{item[4].avg}</tr>
          <tr>{item[5].avg}</tr>
          <tr>{item[6].avg}</tr>
          <tr>{item[7].avg}</tr>
          <tr>{item[8].avg}</tr>
          <tr>{item[9].avg}</tr>
          <tr>{item[10].avg}</tr>
          <tr>{item[11].avg}</tr>
          <tr>{item[12].avg}</tr>
          <tr>{item[13].avg}</tr>
          <tr>{item[14].avg}</tr>
          <tr>{item[15].avg}</tr>
          <tr>{item[16].avg}</tr>
          <tr>{item[17].avg}</tr>
          <tr>{item[18].avg}</tr>
          <tr>{item[19].avg}</tr>
          <tr>{item[20].avg}</tr>
          <tr>{item[21].avg}</tr>
          <tr>{item[22].avg}</tr>
          <tr>{item[23].avg}</tr>
          <tr>{item[24].avg}</tr>
          <tr>{item[25].avg}</tr>
          <tr>{item[26].avg}</tr>
          <tr>{item[27].avg}</tr>
          <tr>{item[28].avg}</tr>
          <tr>{item[29].avg}</tr>
          <tr>{item[30].avg}</tr>
          <tr>{item[31].avg}</tr>
          <tr>{item[32].avg}</tr>
          <tr>{item[33].avg}</tr>
          <tr>{item[34].avg}</tr>
          <tr>{item[35].avg}</tr>
          <tr>{item[36].avg}</tr>
          <tr>{item[37].avg}</tr>
          <tr>{item[38].avg}</tr>
          <tr>{item[39].avg}</tr>
        </td>
      );
    });
    let avg = this.state.avg.map(item => {
      return <tr>{item}</tr>;
    });

    return (
      <Table striped bordered hover responsive variant="dark">
        <thead>
          <tr>
            <th>Generations</th>
            <th>Run#1 Avg. Fit.</th>
            <th>Run#2 Avg. Fit.</th>
            <th>Run#3 Avg. Fit.</th>
            <th>Run#4 Avg. Fit.</th>
            <th>Run#5 Avg. Fit.</th>
            <th>Run#6 Avg. Fit.</th>
            <th>Run#7 Avg. Fit.</th>
            <th>Run#8 Avg. Fit.</th>
            <th>Run#9 Avg. Fit.</th>
            <th>Run#10 Avg. Fit.</th>
            <th>Average Avg. Fit.</th>
          </tr>
        </thead>
        <tbody>
          <td>
            {this.state.rowGenerator.map((item, index) => {
              return <tr>{index + 1}</tr>;
            })}
          </td>
          {table}
          <td>{avg}</td>
        </tbody>
      </Table>
    );
  }
}
