import React, { Component } from "react";
import Table from "react-bootstrap/Table";

export default class TableComp extends Component {
  state = {
    generation: this.props.data,
    rowGenerator: [
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1"
    ],
    first: true,
    avg: []
  };

  componentWillReceiveProps(nextProps) {
    const { survived } = nextProps;
    let sum = 0;
    let avg = [];
    if (survived.length === 10) {
      if (survived[9].length === 41) {
        if (this.state.first) {
          this.setState({ first: false }, () => {
            for (let i = 0; i < 40; i++) {
              survived.map((item, index) => {
                sum += item[i].best.fitness;
              });
              console.log(sum);
              avg.push(sum / 10);
              sum = 0;
            }
          });
          // for (let i = 0; i < 1; i++)
          //   survived.map((item, index) => {
          //     console.log(item[i].best.fitness);
          //   });
          this.setState({ avg: avg });
        }
      }
    }
  }
  render() {
    let table = this.props.survived.map((item, index) => {
      return (
        <td>
          <tr>{item[0].best.fitness}</tr>
          <tr>{item[1].best.fitness}</tr>
          <tr>{item[2].best.fitness}</tr>
          <tr>{item[3].best.fitness}</tr>
          <tr>{item[4].best.fitness}</tr>
          <tr>{item[5].best.fitness}</tr>
          <tr>{item[6].best.fitness}</tr>
          <tr>{item[7].best.fitness}</tr>
          <tr>{item[8].best.fitness}</tr>
          <tr>{item[9].best.fitness}</tr>
          <tr>{item[10].best.fitness}</tr>
          <tr>{item[11].best.fitness}</tr>
          <tr>{item[12].best.fitness}</tr>
          <tr>{item[13].best.fitness}</tr>
          <tr>{item[14].best.fitness}</tr>
          <tr>{item[15].best.fitness}</tr>
          <tr>{item[16].best.fitness}</tr>
          <tr>{item[17].best.fitness}</tr>
          <tr>{item[18].best.fitness}</tr>
          <tr>{item[19].best.fitness}</tr>
          <tr>{item[20].best.fitness}</tr>
          <tr>{item[21].best.fitness}</tr>
          <tr>{item[22].best.fitness}</tr>
          <tr>{item[23].best.fitness}</tr>
          <tr>{item[24].best.fitness}</tr>
          <tr>{item[25].best.fitness}</tr>
          <tr>{item[26].best.fitness}</tr>
          <tr>{item[27].best.fitness}</tr>
          <tr>{item[28].best.fitness}</tr>
          <tr>{item[29].best.fitness}</tr>
          <tr>{item[30].best.fitness}</tr>
          <tr>{item[31].best.fitness}</tr>
          <tr>{item[32].best.fitness}</tr>
          <tr>{item[33].best.fitness}</tr>
          <tr>{item[34].best.fitness}</tr>
          <tr>{item[35].best.fitness}</tr>
          <tr>{item[36].best.fitness}</tr>
          <tr>{item[37].best.fitness}</tr>
          <tr>{item[38].best.fitness}</tr>
          <tr>{item[39].best.fitness}</tr>
        </td>
      );
    });
    let avg = this.state.avg.map(item => {
      return <tr>{item}</tr>;
    });

    return (
      <Table striped bordered hover responsive variant="dark">
        <thead>
          <tr>
            <th>Generations</th>
            <th>Run#1</th>
            <th>Run#2</th>
            <th>Run#3</th>
            <th>Run#4</th>
            <th>Run#5</th>
            <th>Run#6</th>
            <th>Run#7</th>
            <th>Run#8</th>
            <th>Run#9</th>
            <th>Run#10</th>
            <th>Average BSF</th>
          </tr>
        </thead>
        <tbody>
          <td>
            {this.state.rowGenerator.map((item, index) => {
              return <tr>{index + 1}</tr>;
            })}
          </td>
          {table}
          <td>{avg}</td>
        </tbody>
      </Table>
    );
  }
}
