import React, { Component } from "react";
import LineGraph from "smooth-line-graph";

export default class Graph extends Component {
  state = {
    avg: [],
    first: true,
    avgFit: []
  };
  componentWillReceiveProps(nextProps) {
    const { survived } = nextProps;
    let sum = 0;
    let sumFit = 0;
    let avg = [];
    let avgFit = [];
    if (survived.length === 10) {
      if (survived[9].length === 41) {
        if (this.state.first) {
          this.setState({ first: false }, () => {
            for (let i = 0; i < 40; i++) {
              survived.map((item, index) => {
                sum += item[i].best.fitness;
                sumFit += item[i].avg;
              });
              console.log(sumFit);
              avg.push(sum / 10);
              avgFit.push(sumFit);
              sum = 0;
              sumFit = 0;
            }
          });

          this.setState({ avg: avg, avgFit: avgFit });
        }
      }
    }
  }
  render() {
    let avgPlot = this.state.avg.map((item, index) => {
      return [index + 1, item];
    });
    let avgPlotFit = this.state.avgFit.map((item, index) => {
      console.log(item, item / 10);
      const tt = item / 10;
      return [index + 1, tt];
    });
    const props = {
      name: "multi",
      width: 500,
      height: 500,
      padding: [40, 40, 100, 40],
      lines: [
        {
          key: "series1",
          data: avgPlot,
          color: "#03c",
          smooth: false
        },
        {
          key: "series2",
          data: avgPlotFit,
          color: "#c03",
          smooth: true
        }
      ],
      minX: 0,
      minY: 0
    };

    return <LineGraph {...props} />;
  }
}
