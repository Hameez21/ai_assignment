import React , {Component} from 'react'
import {Navbar} from 'react-bootstrap'
import { whileStatement } from '@babel/types';


export default class NavBar extends Component {


    render(){
        return(
        <div style={styles.navContainer}>
            <div style={{alignSelf:'center'}}>
                <a href='#top' style={styles.navText}>Genetic Algorithm</a>
            </div>
        </div>
        )
    }
}


const styles = {
    navContainer:{
        width:'100%',
        minHeight:'3vw',
        backgroundColor:'#263238',
        boxShadow:'0px 3px #111',
        flexDirection:'row',
        display:'flex',
        padding:12,
        paddingLeft:20,
        paddingRight:20
    },
    navText:{
        color:'#fff',
        textDecoration:'none',
        fontSize:28,
        textAlign:'center'
    
    }
}